'use strict'

const UserDatabase = require('../database/user.database')
class UserController{  
    constructor(){}
    async create(id,lastName,firstName,adress){
        if(id != undefined && lastName != undefined && firstName != undefined && adress != undefined){
            const user = await UserDatabase.create(id,lastName,firstName,adress);
            return user;
        }
        return undefined;
    }
    async getUsers(){
        const users = await UserDatabase.getUsers();
        users.forEach(user => {
            console.log(user.id + ", " + user.firstname + ", " + user.name + ", "+ user.adress );
        });

    }
}
module.exports = new UserController();