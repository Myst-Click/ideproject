'use strict'

const sqlite = require("aa-sqlite");
const Database = require("./database")

class UserDatabase extends Database{  
    constructor(){
        super()
        this.initialiseTable();
    }
    async create(id,name,firstname,adress){
        const userExisted = await this.getUserById(id);
        if(!userExisted){
            let query = `INSERT INTO users (id,name,firstname,adress) VALUES( ${id},'${name}','${firstname}','${adress}')`;
            const result = await sqlite.run(query).catch(function(err){
                console.log(err);
            });
            return result;
        }
        return false;
        
    }
    async getUserById(id){
        let query = "SELECT ID, FIRSTNAME, NAME, ADRESS from USERS WHERE ID = " + id;
        const result = await sqlite.get(query).catch(function(err){
            console.error(err);
        })
        return result;
    }
    async getUsers(){
        let query = "SELECT ID, FIRSTNAME, name, adress from users";
        const result = await sqlite.all(query).catch(function(err){
            console.error(err);
        });
        return result;
    }
    async initialiseTable(){
        const query = 'CREATE TABLE IF NOT EXISTS users (' +
                        'id INTEGER PRIMARY KEY,'+
                        'name TEXT NOT NULL,'+
                        'firstname TEXT NOT NULL,'+
                        'adress TEXT NOT NULL)';
        await sqlite.run(query).catch(function(err){
            console.error(err);
        });
    }
}
module.exports = new UserDatabase();