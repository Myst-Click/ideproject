const readline = require('readline');
const fs = require('fs');
const sleep = require('sleep')
const UserController = require('./controller/user.controller');

console.log("Recherche du fichier 'client.txt ...\n");
sleep.sleep(3);
if(fs.existsSync('./client.txt')){
    read()

}
else{
    console.log("impossible de trouver le fichier cible.")
}
async function read(){
    console.log("Liste des clients actuels : \n");
    await UserController.getUsers();
    console.log("\n")
    const readInterface = readline.createInterface({
        input: fs.createReadStream('./client.txt'),
        console: false
    });
    let row = 0;
    let rowInserted = 0;
    return await new Promise((resolve,reject)=>{
        readInterface.on('line', async(line)=> {
            row++;
            const lineArray = line.split(';');
            console.log("Insertion des données pour la ligne : "+row);
            sleep.sleep(2)
            if(await validLine(lineArray)){
                await UserController.create(lineArray[0],lineArray[1],lineArray[2],lineArray[3]); 
            }
            
        }).on('close',async()=>{
            console.log("\n")
            console.log("Insertion terminé.\n");
        });
        resolve();
    })
}
async function validLine(array){
    if(array.length != 4){
        console.log("Des champs sont manquants");
        return false;
    }
    const parsed = parseInt(array[0], 10);
    if (isNaN(parsed)) { return false; }
    return true;
}

